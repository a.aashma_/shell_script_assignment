#!/bin/bash
cd /home/aashma/Desktop/bajra/shell_script_assignment
DAY=$(date +%F)s

echo "Please enter the file extension:"
    read EXTENSION

echo "Please enter the prefix: (press enter for $DAY)"
    read

for NAME in *.$EXTENSION
    do
        echo "Renaming $NAME to ${DAY}-${NAME}"
        mv $NAME ${DAY}-${NAME}
    done